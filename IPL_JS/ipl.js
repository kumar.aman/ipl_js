let matches = require('../IPL_JS/matches.json');
let deliveries = require('../IPL_JS/deliveries.json');

function findMatchesPerYear(matches) {
  let iplMatchesPerYear = matches.reduce((iplMatchesPerYear, currentValue) => {

    if (iplMatchesPerYear.hasOwnProperty(currentValue.season)) {
      iplMatchesPerYear[currentValue.season] += 1;
    } else {
      iplMatchesPerYear[currentValue.season] = 1;
    }
    return iplMatchesPerYear;
  }, {})
  return iplMatchesPerYear;
}
const iplMatchesPerYear = findMatchesPerYear(matches);
console.log(iplMatchesPerYear);

function findNumberOfMatchesWonByTeam(matches){
  let iplMatchResults = matches.reduce((iplMatchResults,currentMatch) => {

    if (iplMatchResults.hasOwnProperty(currentMatch.winner)){
      iplMatchResults[currentMatch.winner]+=1;
    } else {
      iplMatchResults[currentMatch.winner] = 1;
    }
    return iplMatchResults;    
  }, {})
  return iplMatchResults;
}
const iplMatchResults = findNumberOfMatchesWonByTeam(matches);
console.log(iplMatchResults);

function findExtraRunsConcededFor2016(matches,deliveries){
    let matchIdOf2016 =matches.filter((match)=> match.season == 2016).map((match) => parseInt(match.id));

    let extraRunsConcededByTeam = deliveries.reduce((extraRunsConcededByTeam,currentBall) => {

    let matchId =parseInt(currentBall.match_id);
    if(matchIdOf2016.includes(matchId)){
      if (extraRunsConcededByTeam.hasOwnProperty(currentBall.bowling_team)){
        extraRunsConcededByTeam[currentBall.bowling_team]+= parseInt(currentBall.extra_runs);    
      } else{
      extraRunsConcededByTeam[currentBall.bowling_team]= parseInt(currentBall.extra_runs);
      }
    }
    return extraRunsConcededByTeam;     
  },{})
  return extraRunsConcededByTeam;
}
const extraRunsConcededByTeam = findExtraRunsConcededFor2016(matches,deliveries);
console.log(extraRunsConcededByTeam);

function findMostEconomicalBowlerIn2015(matches,deliveries){  
  let matchIdOf2015 =matches.filter((match)=> match.season == 2015).map((match) => parseInt(match.id));

  let bowlers = deliveries.reduce((bowlers,currentBall) => {

    let matchId = parseInt(currentBall.match_id)
    if (matchIdOf2015.includes(matchId)) {
      if(bowlers.hasOwnProperty(currentBall.bowler)) {
        bowlers[currentBall.bowler].balls += 1;        
        bowlers[currentBall.bowler].runs += parseInt(currentBall.total_runs);
        let totalRun = bowlers[currentBall.bowler].balls += 1;
        let totalBall = bowlers[currentBall.bowler].runs += parseInt(currentBall.total_runs);
        bowlers[currentBall.bowler].economy = (totalRun/(totalBall / 6));
      } else {
        bowlers[currentBall.bowler] = {"runs": parseInt(currentBall.total_runs), "balls" : 1, "economy" : 0.00 };      
      }
    }
    return bowlers;
  },{})
  
  const allEconomy=[];
  for(let bowlerName in bowlers){
    if (bowlers[bowlerName].economy <= 10){
      allEconomy.push(bowlers[bowlerName].economy);
    }
  }
  allEconomy.sort();
  const topEconomyBowler ={};
  for  (key in bowlers) {
    if (bowlers[key].economy == allEconomy[0]){
      topEconomyBowler[key] = { economyRate: bowlers[key].economy };
    }
  }
  return topEconomyBowler;
}
const topEconomyBowler = findMostEconomicalBowlerIn2015(matches,deliveries);
console.log(topEconomyBowler);

function findNumberOfTimeHyderabadHosted(matches) {
  let numberOfTimeHyderabadHosted = matches.filter((match) => match.city == "Hyderabad");
  return numberOfTimeHyderabadHosted;
}
const numberOfTimeHyderabadHosted = findNumberOfTimeHyderabadHosted(matches);
console.log("Number of time Hyderabad hosted : "+numberOfTimeHyderabadHosted.length);
  

  